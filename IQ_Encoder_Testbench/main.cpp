//* General
#include "BufferedSerial.h" // Buffered Serial
#include "Encoder.h"        // Optical Encoder
#include "mbed.h"           //mbed OS
#include "math.h"  

//* IQ Communication Libraries
#include "multi_turn_angle_control_client.hpp" // Clients that speaks to module’s objects
#include "propeller_motor_control_client.hpp"
#include <brushless_drive_client.hpp> // Clients that speaks to module’s objects
#include <generic_interface.hpp>      // Message forming interface

#define TIM TIM2 // Timer used for encoder
#define PI 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348

//-----------------------------Settings------------------------------------//

#define SAMPLE_PERIOD 0.01 // sampling time [s]
#define W 2*PI      // IQ Motor Speed [rad/s]
#define CPR 20000 // Optical Encoder CPR [-]

//-------------------------------------------------------------------------//

//* Initilize Hardware
DigitalOut led1(LED1);    // status LED

DigitalIn activate(D6,PullDown);  // Activation switch
InterruptIn counter(D10); // Encoder index tick


Serial pc(USBTX, USBRX);        // PC serial connection
BufferedSerial IQ(PA_9, PA_10); // Serial connection to IQ

//* Initialize IQ motor
GenericInterface com;
//MultiTurnAngleControlClient mot_speed(0);
BrushlessDriveClient drive(0);
PropellerMotorControlClient prop(0);


//* Initialize Optical encoder
TIM_HandleTypeDef timer1;
TIM_Encoder_InitTypeDef encoderAB;

//* Initialize Measurement timer
LowPowerTicker tick;
LowPowerTimer t;

uint32_t countAB = 0; // quadrature count optical encoder
uint16_t countI = 0;  // index count optical encoder
int8_t dir;           // rotation direction optical encoder
int8_t count_error = 0;
uint32_t count_margin = 5;

int8_t state;
volatile int8_t sample_flag = 0;
double_t IQ_0 = 0;  // IQ angle calibration -> IQ_real = IQ_received - IQ_0
double_t Enc_0 = 0; // Encoder angle calibration -> Enc_real = Enc_received - Enc_0
double_t IQ_raw;
double_t IQ_angle;
double_t ENC_angle;

float velocity_setpoint;

void sample()
{
    sample_flag = (sample_flag == 1) ? 0 : 1;
}

void counter_reset()
{
      TIM->CNT = 0x00000000;

}

void Count_Index() {

  countAB = TIM->CNT;
  dir = TIM->CR1 & TIM_CR1_DIR;
  //led1 = 1;

  counter_reset();

  if (dir == 0) {
    countI++;
  } else {
    countI--;
  }

  if (countAB < (CPR - count_margin) || countAB > (CPR + count_margin)) {

      count_error = 1; // raise flag if pulse count is wrong
      //led1 = 0;
  }
  
}

float Enc_get_angle() {

  int32_t count = TIM->CNT;
  double_t angle = fmod(abs(count),CPR) / CPR * 2 * PI;

  return angle;
}

void IQ_set_speed(float velocity_setpoint) {
  uint8_t communication_buffer_tx[64];
  uint8_t communication_length_tx;

  //* Generate set message(s)
  //mot_speed.ctrl_velocity_.set(com, velocity_setpoint);
  prop.ctrl_velocity_.set(com, velocity_setpoint);
  prop.timeout_.set(com, 3600);


  //* Sent message(s)
  if (com.GetTxBytes(communication_buffer_tx, communication_length_tx)) {
    // Use serial hardware to send messages
    IQ.write(communication_buffer_tx, communication_length_tx);
  }
}

float IQ_query(float velocity_setpoint) {
  uint8_t communication_buffer_tx[64];
  uint8_t communication_length_tx;
  uint8_t communication_buffer_rx[64];
  uint8_t rx_byte_index = 0;

  //* Generate get message(s)
  //mot_speed.obs_angular_displacement_.get(com);
  //mot_speed.ctrl_velocity_.set(com, velocity_setpoint);
  prop.ctrl_velocity_.set(com, velocity_setpoint);
  prop.timeout_.set(com, 60);
  drive.obs_angle_.get(com);


  //* Sent message(s)
  if (com.GetTxBytes(communication_buffer_tx, communication_length_tx)) {
    // Use serial hardware to send messages
    IQ.write(communication_buffer_tx, communication_length_tx);
  }

  //* Receive Message
  while (IQ.readable()) {
    communication_buffer_rx[rx_byte_index] = IQ.getc();
    rx_byte_index++;
  }

  //* Puts the recently read bytes into coms receive queue
  com.SetRxBytes(communication_buffer_rx, rx_byte_index);

  uint8_t *rx_data;  // temporary pointer to received type+data bytes
  uint8_t rx_length; // number of received type+data bytes

  //* while we have message packets to parse
  while (com.PeekPacket(&rx_data, &rx_length)) {
    // Share that packet with all client objects
    drive.ReadMsg(rx_data, rx_length);
    // Once were done with the message packet, drop it
    com.DropPacket();
  }

  //* output reply
  //IQ_raw = mot_speed.obs_angular_displacement_.get_reply();
  IQ_raw = drive.obs_angle_.get_reply();
  double_t angle = abs(fmod(IQ_raw - IQ_0, (2 * PI) ));

  return angle;
}


void setup() {
  // Initialize Input Pins
  //activate.mode(PullDown);
  counter.mode(PullDown);
  counter.enable_irq();
  activate.mode(PullDown);
  
 

  // Initialize Serial Communication
  pc.baud(115200);
  IQ.baud(115200);

  // Initialize Optical Encoder
  EncoderInit(&encoderAB, &timer1, TIM, 0xffffffff, TIM_ENCODERMODE_TI12);
  // configure index pin interrupt
  counter.rise(&Count_Index);
  counter.enable_irq();

  // Initialize Sample Timer
  tick.attach(&sample, SAMPLE_PERIOD);
  t.start();


  // calibrate

  IQ_0 = 0;

while (IQ_0 == 0){
    IQ_query(0);
    IQ_0 = IQ_raw;
}
  
 
}


int main() {

  setup(); //run setup

  
  //t.reset();
  // start timer


  while (true) {

    int on = activate.read();

    if (on==1){
        velocity_setpoint = 4*PI;
        led1 = 1;
    }
    else {
        velocity_setpoint = 0;
        led1 = 0;
     }


    IQ_angle  = IQ_query(velocity_setpoint);
    ENC_angle = Enc_get_angle();

   
    if (sample_flag == 1){
        int32_t time_stamp = t.read_ms();
        //printf("Number of full rotations: %d, Last CPR: %d, Dir: %d,\n\r", countI, countAB, dir);
        //pc.printf("Motor angle (IQ - Enc)  (%f - %f) Diff: %f\n\r",IQ_angle, ENC_angle, abs(IQ_angle - ENC_angle));
        //pc.printf("Motor angle (IQ - Enc)  (%f - %f) Diff: %f\n\r",IQ_angle, ENC_angle, abs(IQ_angle - ENC_angle));
        //pc.printf("IQ raw:%f\n\r",IQ_raw);
        pc.printf("Time:%i AngleIQ:%f AngleENC:%f\n\r",time_stamp,IQ_angle, ENC_angle);
       // pc.printf("IQ readout time: %i \n\r",duration_ms);
        
        sample_flag = 0;
    }

    

  }
}
