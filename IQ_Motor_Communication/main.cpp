/*
* IQ Motion Control spin and report demo.
*
* This code will command a motor to spin at various voltages and
* simultaniously report the motor’s position and velocity over USB
*
*
* The circuit:
* Serial1 RX is directly connected to motor TX (Red)
* Serial1 TX is directly connected to motor RX (White)
*
* Created 2018/10/8 by Matthew Piccoli
*
* This example code is in the public domain.
*/

#include "mbed.h"
#include <generic_interface.hpp> // Message forming interface
#include <brushless_drive_client.hpp> // Clients that speaks to module’s objects
#include "multi_turn_angle_control_client.hpp" // Clients that speaks to module’s objects
#include "BufferedSerial.h"


// USER SETABLE VALUES HERE----------------------------------------------------



// END USER SETABLE VALUES-----------------------------------------------------

//void SendMessages(float angle_rate_command);
void DoSomethingWithMessages(void);
void Send_Messages(void);
void Receive_Messages(void);


// Make a communication interface object
GenericInterface com;
// Make a objects that talk to the module
MultiTurnAngleControlClient mot_speed(0);

// Serial interfaces
//Serial pc(PA_9,PA_10);
Serial pc (USBTX,USBRX);
BufferedSerial IQ (PA_9, PA_10);
Timer t;


// This buffer is for passing around messages.
uint8_t communication_buffer_tx[64];
uint8_t communication_buffer_rx[64];
uint8_t communication_length_tx;
uint8_t communication_length_rx;

float_t speed;
float_t speed_0;
int32_t dt1 = 0;

int message_received_flag = 0;
int message_sent_flag = 0;


int main() {
    
    float angular_velocity_setpoint = 30; // angular velocity setupoint [rad/s?]
    // Initialize USB communicaiton
    pc.baud(115200);
    pc.printf("Program starting\n\r");
    // Initialize the Serial peripheral for motor controller
    IQ.baud(115200);

    // Initialize Timer
    t.start();
   

    while (true)
    {
        speed_0 = speed;

        t.reset();

        Send_Messages();
        
        while (not mot_speed.obs_angular_velocity_.IsFresh()){
            Receive_Messages();
        }

        speed =  mot_speed.obs_angular_velocity_.get_reply();
        dt1 = t.read_us();

        pc.printf("\r\n IQ return trip latency [us]: %i ", dt1);

        wait(0.1);
    }
}


void Send_Messages(void)
{
    float angle_rate_command = 10;

    //** Generate set message(s)
    mot_speed.ctrl_velocity_.set(com, angle_rate_command);

    //** Generate get message(s)
    mot_speed.obs_angular_velocity_.get(com);
    
    if(com.GetTxBytes(communication_buffer_tx,communication_length_tx))
    {
        //Use serial hardware to send messages
        IQ.write(communication_buffer_tx,communication_length_tx);
    }
}


void Receive_Messages()
{
    uint8_t communication_buffer[64];
    uint8_t i=0;

    

    while(IQ.readable())
        {
            communication_buffer[i] = IQ.getc();
            i++;
        }
    
    communication_length_rx = i;

    //Puts the recently read bytes into coms receive queue
    com.SetRxBytes(communication_buffer,i);

    uint8_t *rx_data; // temporary pointer to received type+data bytes
    uint8_t rx_length; // number of received type+data bytes

    //while we have message packets to parse
    while(com.PeekPacket(&rx_data,&rx_length))
    {
    // Share that packet with all client objects
    mot_speed.ReadMsg(rx_data,rx_length);
    
    // Once were done with the message packet, drop it
    com.DropPacket();
    }
}
