/*
 * Using STM32's counter peripherals to interface rotary encoders.
 * Encoders are supported on F4xx's TIM1,2,3,4,5. TIM2 & TIM5 have 32bit count, others 16bit.
 * Beware mbed uses TIM5 for system timer, SPI needs TIM1, others used for PWM.
 * Check your platform's PeripheralPins.c & PeripheralNames.h if you need both PWM & encoders.
 *
 * Edit HAL_TIM_Encoder_MspInitFx.cpp to suit your mcu & board's available pinouts & pullups/downs.
 *
 * Thanks to:
 * http://petoknm.wordpress.com/2015/01/05/rotary-encoder-and-stm32/
 *
 * References:
 * http://www.st.com/st-web-ui/static/active/en/resource/technical/document/user_manual/DM00122015.pdf
 * http://www.st.com/st-web-ui/static/active/en/resource/technical/document/reference_manual/DM00096844.pdf
 * http://www.st.com/web/en/resource/technical/document/application_note/DM00042534.pdf
 * http://www.st.com/web/en/resource/technical/document/datasheet/DM00102166.pdf
 * 
 * David Lowe Jan 2015
 */

#include "mbed.h"
#include "Encoder.h"

#define TIM TIM2


TIM_Encoder_InitTypeDef encoderAB;
TIM_HandleTypeDef  timer2;

Serial pc(USBTX,USBRX);
    
InterruptIn counter(A5);

uint32_t countAB=0;
uint16_t countI =0;
int8_t dir;

void Count_Index() {
  
    countAB = TIM->CNT;
    dir     = TIM->CR1&TIM_CR1_DIR;

    if (dir==0) {
        countI++;
    } else {
        countI--;
    }

    TIM->CNT = 0x00000000;
}


int main(){

    
    // Configure serial monitor
    pc.baud(115200);

    //counting on both A&B inputs, 4 ticks per cycle, full 32-bit count
    EncoderInit(&encoderAB, &timer2, TIM, 0xffffffff, TIM_ENCODERMODE_TI12);

    //configure index pin interrupt
    counter.rise(&Count_Index);
    
    while(1) {


        //OK 401 411 446 NOK 030
        //pc.printf("Counter:%i",TIM2-> CNT);
        //dir2   = TIM2-> CR1 & TIM_CR1_DIR;

        //countAB = __HAL_TIM_GET_COUNTER(&timer2);
        //dir     = __HAL_TIM_IS_TIM_COUNTING_DOWN(&timer2);


        //printf("%d%s %d\n\r", countAB, dir==0 ? "+":"-",countI);
        printf("\n\rNumber of full rotations: %i - CPR: %i", countI, countAB);
        //printf("\n\rCNT: %i, dir: %i\n\r", TIM2->CNT, (TIM2->CR1&TIM_CR1_DIR) ? 0:1);
        wait(0.5);
    }
}
